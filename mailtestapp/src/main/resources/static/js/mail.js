function sendMail() {
    hideErrors();

    $.post($("#sendMailUrl").attr("href"),
        {
            recipientAddress: $('#recipientAddress').val(),
            messageBody:$('#messageBody').val()
        }
    ).done(function (resultData) {
            showMessageBar("messageBar", resultData.status, resultData.message);
            if (resultData.status !== "SUCCESS") {
                if (resultData.recipientAddressError === true) {
                    $("#recipientAddressErrorMessage").show();
                }
            }
        }

    ).fail(function (resultData) {
        showMessageBar("messageBar", "ERROR", "Viga, saatmisel!");
    });
}

function hideErrors() {
    hideMessageBar("messageBar");
    $("#recipientAddressErrorMessage").hide();
}



function showMessageBar(messageBarDivId, messageType, message) {

    var messageClass = "alert alert-info";

    switch(messageType) {

        case "SUCCESS":
            messageClass = "alert alert-success";
            break;

        //case "INFO":
        //messageClass = "alert alert-info";
        //break;

        case "WARNING":
            alertClass = "alert alert-warning";
            break;

        case "ERROR":
            messageClass = "alert alert-danger";
            break;
    }

    var stringBuildStart = "<div class=\"row\"><div class=\"col-md-12\"><div class=\"" + messageClass + "\" role=\"alert\">";
    var stringBuildEnd = "</div></div>";

    $("#" + messageBarDivId).html(stringBuildStart + message + stringBuildEnd);
    $("#" + messageBarDivId).show();
}

function hideMessageBar(messageBarDivId){
    $("#" + messageBarDivId).hide();
}


