

//drop table sent_mail if exists;

create table sent_mail (
    id bigint auto_increment,
    sent_time timestamp not null,
    recipient_address varchar(80) not null,
    message_body varchar(80) null,
);

