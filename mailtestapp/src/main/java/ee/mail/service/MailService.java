package ee.mail.service;

import ee.mail.dao.entity.SentMailEntity;
import org.apache.log4j.Logger;
import ee.mail.dao.SentMailDao;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class MailService {

    private static final Logger logger = Logger.getLogger(MailService.class);

    @Autowired
    SentMailDao sentMailDao;

    public boolean sendMail(String recipientAddress, String recipientName, String subject, String messageBody) {

        try {

            Date sentTime = new Date();

            if (logger.isDebugEnabled()) {
                logger.error("sendMail: " + recipientAddress + " " + sentTime + " " + messageBody);
            }

            /*if (mailConfiguration.getMailHostName() != null && !mailConfiguration.getMailHostName().isEmpty()) {
                Properties properties = System.getProperties();
                properties.put("mail.smtp.host", mailConfiguration.getMailHostName());
                properties.put("mail.sender.name", mailConfiguration.getMailSenderName());
                properties.put("mail.sender.from", mailConfiguration.getMailSenderEmailAddress());
                properties.put("mail.smtp.ssl.trust", mailConfiguration.getMailHostName());
                properties.put("mail.smtp.starttls.enable", "true");
                Session session = Session.getInstance(properties, null);
                Message message = new MimeMessage(session);
                message.setFrom(new InternetAddress(mailConfiguration.getMailSenderEmailAddress(),
                        mailConfiguration.getMailSenderName()));
                message.setRecipient(Message.RecipientType.TO, new InternetAddress(recipientAddress,
                        recipientName, "UTF-8"));
                message.setSubject(subject);
                message.setContent(messageBody, "text/html; charset=utf-8");
                Transport.send(message);

                sentTime = message.getSentDate();
            }*/

            SentMailEntity sentMail = new SentMailEntity(recipientAddress, messageBody, sentTime);
            sentMailDao.create(sentMail);
       } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error("Exception: " + e);
            }
            return false;
        }
        return true;
    }

    public List<SentMailEntity> listSentMails () {
        return sentMailDao.getAllSentMails();
    }

    public List<String> listRecipients() {
        return sentMailDao.getRecipients();
    }

    public List<SentMailEntity> getSentMailsByRecipientAddress(String recipientAddress) {
        return sentMailDao.getSentMailsByRecipientAddress(recipientAddress);
    }

    public SentMailEntity getSentMail(Long id) {
        return sentMailDao.getSentMail(id);
    }
}

