package ee.mail.webapp.controller;

import java.io.IOException;
import java.util.List;

import ee.mail.model.SendMailModel;
import ee.mail.dao.entity.SentMailEntity;
import ee.mail.service.MailService;
import org.apache.commons.validator.routines.EmailValidator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletResponse;

@Controller
public class IndexController {

	@Autowired
	MailService mailService;

    private static final Logger logger = Logger.getLogger(IndexController.class);

	@ResponseStatus(HttpStatus.OK)
	@RequestMapping("/")
	public String mail(Model model) {
		model.addAttribute("sendmailformActive", "active");
    	return "mail/sendmailform";
	}

	@ResponseStatus(HttpStatus.OK)
	@RequestMapping("/sentmails")
	public String sentmails(Model model) {
		List<String> maillist = mailService.listRecipients();
		model.addAttribute("sentmailsActive", "active");
		model.addAttribute("maillist", maillist);
		return "mail/maillist";
	}

	@ResponseStatus(HttpStatus.OK)
	@RequestMapping("/sentmailsbyaddress")
	public String sentmailsbyaddress(
			@RequestParam(value = "recipientAddress", required = true) String recipientAddress,
			Model model) {

		List<SentMailEntity> maillist = mailService.getSentMailsByRecipientAddress(recipientAddress);
		model.addAttribute("sentmailsActive", "active");
		model.addAttribute("recipientaddress", recipientAddress);
		model.addAttribute("maillist", maillist);
		return "mail/maillistbyaddress";
	}

	@ResponseStatus(HttpStatus.OK)
	@RequestMapping("/sentmail")
	public String sentmail(
			@RequestParam(value = "id", required = true) String Id,
			Model model) {

		Long id = Long.parseLong(Id);
		SentMailEntity mail = mailService.getSentMail(id);
		model.addAttribute("sentmailsActive", "active");
		model.addAttribute("sentmail", mail);
		return "mail/sentmail";
	}



	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/sendmail", method = RequestMethod.POST)
	public @ResponseBody
	SendMailModel sendMail(HttpServletResponse response,
						   @RequestParam(value = "recipientAddress", required = true) String recipientAddress,
						   @RequestParam(value = "messageBody", required = false) String messageBody) throws IOException {

		SendMailModel sendMailModel = new SendMailModel();

		if (!EmailValidator.getInstance().isValid(recipientAddress)) {
            if (logger.isDebugEnabled()) {
                logger.error("E-maili adressaat vigane!");
            }
			sendMailModel.setStatus("ERROR");
			sendMailModel.setRecipientAddressError(true);
			sendMailModel.setMessage("E-maili adressaat vigane!");
			return sendMailModel;
		}

		if (!mailService.sendMail(recipientAddress, "Test Klient", "Lihtsalt testime", messageBody)) {
            if (logger.isDebugEnabled()) {
                logger.error("Viga maili saatmisel!");
            }
			sendMailModel.setStatus("ERROR");
			sendMailModel.setMessage("Viga e-maili saatmisel!");
			return sendMailModel;
		}
		return sendMailModel;
	}
}

