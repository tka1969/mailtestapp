package ee.mail.model;

public class SendMailModel {

    private String status = "SUCCESS";
    private String message = "E-mail saadetud";
    private boolean recipientAddressError = false;

    public SendMailModel() {
    }

    public SendMailModel(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public boolean isRecipientAddressError() {
        return recipientAddressError;
    }

    public void setRecipientAddressError(boolean recipientAddressError) {
        this.recipientAddressError = recipientAddressError;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
