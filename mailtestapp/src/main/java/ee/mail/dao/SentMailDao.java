package ee.mail.dao;


import ee.mail.dao.entity.SentMailEntity;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.stereotype.Repository;
import javax.persistence.*;
import java.util.List;

@Repository
public class SentMailDao {

    @PersistenceContext
    private EntityManager entityManager;

    public void create(SentMailEntity sentMail) {
        entityManager.persist(sentMail);
    }

    public List<SentMailEntity> getAllSentMails() {
        return entityManager.createNamedQuery(SentMailEntity.class.getSimpleName() + ".findAllSentMails", SentMailEntity.class).getResultList();
    }

    public List<String> getRecipients() {
        return entityManager.createNamedQuery(SentMailEntity.class.getSimpleName() + ".findRecipients", String.class).getResultList();
    }

    public List<SentMailEntity> getSentMailsByRecipientAddress(String recipientAddress) {
        String namedQuery = SentMailEntity.class.getSimpleName() + ".findSentMailsByRecipientAddress";
        TypedQuery<SentMailEntity> query = entityManager.createNamedQuery(namedQuery, SentMailEntity.class);
        query.setParameter("recipientAddress", recipientAddress);
        return query.getResultList();
    }

    public SentMailEntity getSentMail(Long id) {
        String namedQuery = SentMailEntity.class.getSimpleName() + ".findSentMail";
        TypedQuery<SentMailEntity> query = entityManager.createNamedQuery(namedQuery, SentMailEntity.class);
        query.setParameter("id", id);
        return DataAccessUtils.singleResult(query.getResultList());
    }
}
