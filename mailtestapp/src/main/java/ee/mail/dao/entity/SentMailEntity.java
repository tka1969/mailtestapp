
package ee.mail.dao.entity;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;


import javax.persistence.*;

@NamedQueries(
    {
        @NamedQuery(
                name="SentMailEntity.findAllSentMails",
                query = "SELECT e FROM SentMailEntity e"
        ),
        @NamedQuery(
                name="SentMailEntity.findRecipients",
                query = "SELECT e.recipientAddress FROM SentMailEntity e GROUP BY e.recipientAddress"
        ),
        @NamedQuery(
                name="SentMailEntity.findSentMailsByRecipientAddress",
                query = "SELECT e FROM SentMailEntity e WHERE e.recipientAddress=:recipientAddress"
        ),
        @NamedQuery(
                name="SentMailEntity.findSentMail",
                query = "SELECT e FROM SentMailEntity e WHERE e.id=:id"
        )
    }
)
@Entity
@Table(name = "sent_mail")
public class SentMailEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "recipient_address")
    private String recipientAddress;

    @Column(name = "message_body", nullable=true)
    private String messageBody;

    @Column(name = "sent_time")
    private Timestamp sentTime;

    public SentMailEntity() {
    }

    public SentMailEntity(String recipientAddress, String messageBody, Date sentTime) {
        this.setRecipientAddress(recipientAddress);
        this.setMessageBody(messageBody);
        this.setSentTime(sentTime);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getRecipientAddress() {
        return recipientAddress;
    }

    public void setRecipientAddress(String recipientAddress) {
        this.recipientAddress = recipientAddress;
    }


    public String getMessageBody() {
        return messageBody;
    }

    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }


    public Date getSentTime() {
        return new Date(sentTime.getTime());
    }

    public void setSentTime(Date sentTime) {
        this.sentTime = new Timestamp(sentTime.getTime());
    }

    public String getIdAsString() {
        return id.toString();
    }

    public String getSentTimeAsString() {
        return new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(this.sentTime);
    }

    public String getMessageBodyTrunked(int length) {

        if (messageBody.length() > 0) {
            String lines[] = messageBody.split("\\r\\n|\\n|\\r");
            if (lines[0].length() > length)
                return lines[0].substring(0, length);
            return lines[0];
        }
        return messageBody;
    }
}

