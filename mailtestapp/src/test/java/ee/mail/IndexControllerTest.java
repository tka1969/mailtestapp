package ee.mail;

import ee.mail.dao.entity.SentMailEntity;
import ee.mail.webapp.controller.IndexController;
import ee.mail.service.MailService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import static org.hamcrest.Matchers.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@RunWith(SpringRunner.class)
@ContextConfiguration(classes = SpringBootWebApplication.class)
@SpringBootTest
public class IndexControllerTest {

    @Mock
    private MailService mailServiceMock;

    @InjectMocks
    private IndexController indexController;

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        // Process mock annotations
        MockitoAnnotations.initMocks(this);
        // Setup Spring test in standalone mode
        this.mockMvc = MockMvcBuilders.standaloneSetup(indexController).build();
    }

    @Test
    public void testsendMail() throws Exception {
        when(mailServiceMock.sendMail(any(String.class), any(String.class), any(String.class), any(String.class)))
            .thenReturn(true);
        this.mockMvc.perform(post("/sendmail")
                .param("recipientAddress", "mvcemail@test.com")
                .param("messageBody", "mvcfirst"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON ))
                .andExpect(jsonPath("$.status", is("SUCCESS")))
                .andExpect(jsonPath("$.message", is("E-mail saadetud")))
                .andExpect(jsonPath("$.recipientAddressError", is(false)));
    }


    @Test
    public void testSentMails() throws Exception {
        List<String> maillist = new ArrayList<String>();
        maillist.add(new String("test@mail.com"));
        maillist.add(new String("test@oom.com"));
        when(mailServiceMock.listRecipients()).thenReturn(maillist);
        this.mockMvc.perform(get("/sentmails"))
                .andExpect(status().isOk())
                .andExpect(forwardedUrl("mail/maillist"))
                .andExpect(model().attribute("maillist", maillist));
    }

    @Test
    public void testSentMailsByAddress() throws Exception {
        List<SentMailEntity> maillist = new ArrayList<SentMailEntity>();
        maillist.add(new SentMailEntity("test@mail.com", "test1", new Date()));
        maillist.add(new SentMailEntity("test@mail.com", "test2", new Date()));
        when(mailServiceMock.getSentMailsByRecipientAddress(any(String.class))).thenReturn(maillist);
        this.mockMvc.perform(get("/sentmailsbyaddress")
                .param("recipientAddress", "test@mail.com"))
                .andExpect(status().isOk())
                .andExpect(forwardedUrl("mail/maillistbyaddress"))
                .andExpect(model().attributeExists("recipientaddress"))
                .andExpect(model().attribute("maillist", maillist));
    }

    @Test
    public void testSentMail() throws Exception {
        SentMailEntity mail = new SentMailEntity("test@mail.com", "test1", new Date());
        when(mailServiceMock.getSentMail(any(Long.class))).thenReturn(mail);
        this.mockMvc.perform(get("/sentmail")
                .param("id", "1"))
                .andExpect(status().isOk())
                .andExpect(forwardedUrl("mail/sentmail"))
                .andExpect(model().attribute("sentmail", mail));
    }
}
