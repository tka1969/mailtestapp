package ee.mail.service;

import ee.mail.SpringBootWebApplication;
import ee.mail.dao.entity.SentMailEntity;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = SpringBootWebApplication.class)
@SpringBootTest
public class MailServiceTest {

    @Autowired
    EntityManagerFactory entityManagerFactory;

    @Autowired
    MailService mailService;

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() throws IOException {
    }

    @Before
    public void setUp() {
        EntityManager em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        // group 1
        em.persist(new SentMailEntity("test@mail.com", "test1", new Date()));
        em.persist(new SentMailEntity("test@mail.com", "test2", new Date()));
        // group 2
        em.persist(new SentMailEntity("test@oom.com", "test1", new Date()));
        em.getTransaction().commit();
    }

    @After
    public void tearDown() throws IOException {
        EntityManager em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        em.createNativeQuery("TRUNCATE TABLE sent_mail").executeUpdate();
        em.createNativeQuery("ALTER TABLE sent_mail ALTER COLUMN id RESTART WITH 1").executeUpdate();
        em.getTransaction().commit();
    }

    @Test
    public void listSentMails () {
        System.out.println("MailServiceTest: listSentMails");
        List<SentMailEntity> maillist = mailService.listSentMails();
        assertEquals(3, maillist.size());
    }

    @Test
    public void listRecipients() {
        System.out.println("MailServiceTest: listRecipients");
        List<String> maillist = mailService.listRecipients();
        assertEquals(2, maillist.size());
    }

    @Test
    public void getSentMailsByRecipientAddress() {
        System.out.println("MailServiceTest: getSentMailsByRecipientAddress");
        List<SentMailEntity> maillist = mailService.getSentMailsByRecipientAddress("test@oom.com");
        assertEquals(1, maillist.size());
    }

    @Test
    public void getSentMail() {
        System.out.println("MailServiceTest: getSentMail");
        SentMailEntity sentMail = mailService.getSentMail(1L);
        assertNotEquals(null, sentMail);
    }
}
