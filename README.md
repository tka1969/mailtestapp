


E-maili saatmise rakendus on realiseeritud IntelliJ IDEA projektina.
Rakendus vajab tööks tomcat rakenduste serverit, mis on saadaval aadressilt http://tomcat.apache.org/.


Rakendus kasutab H2 andmebaasi. H2 konsooli saab logida lingilt http://localhost:8080/h2 (http://host:port/h2). 
Klikates vasakul olevale SENT_MAIL tabelile kuvatakse päringu aknas select sql. Run nupp näitab päringu tulemusena tabelisse lisatud kirjeid.


NB! Kuna on valitud H2 mälus olev variant, siis andmed hävinevad rakenduse töö lõpetamisel.


Rakenduse saab lihtsalt käivitada IntelliJ IDEA-st Run->Run 'Rakendus'.


